package com.acmebank.accountmanager.controller;

import com.acmebank.accountmanager.exception.InvalidAccountNumberException;
import com.acmebank.accountmanager.model.dto.TransferDto;
import com.acmebank.accountmanager.service.AccountService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/account")
public class AccountController {
    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("/transfer")
    public String transfer(@RequestBody TransferDto dto) {
        try {
            accountService.transfer(dto.getFromAccountNumber(), dto.getToAccountNumber(), dto.getAmount());
            return "The transfer is successful.";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @GetMapping("/getBalance")
    public double getBalance(@RequestParam Long accountNumber) throws InvalidAccountNumberException {
        return accountService.getAccountBalance(accountNumber);
    }
}
