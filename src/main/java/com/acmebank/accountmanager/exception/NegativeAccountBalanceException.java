package com.acmebank.accountmanager.exception;

public class NegativeAccountBalanceException extends BaseException {
    public NegativeAccountBalanceException(Long accountNumber, double debitAmount, double balance) {
        super(String.format("%d accountNumber failed to debit %f from balance %f", accountNumber, debitAmount, balance));
    }
}
