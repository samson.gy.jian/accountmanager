package com.acmebank.accountmanager.exception;

public class BaseException extends Exception{
    public BaseException(String message){
        super(message);
    }
}
