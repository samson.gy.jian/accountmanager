package com.acmebank.accountmanager.exception;

public class InvalidAccountNumberException extends BaseException{
    public InvalidAccountNumberException(Long accountNumber) {
        super("AccountNumber " + accountNumber + " is invalid.");
    }
}
