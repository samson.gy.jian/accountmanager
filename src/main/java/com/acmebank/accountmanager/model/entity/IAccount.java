package com.acmebank.accountmanager.model.entity;

public interface IAccount {
    void credit(double amount);
    boolean debit(double amount);
}
