package com.acmebank.accountmanager.service;

import com.acmebank.accountmanager.exception.InvalidAccountNumberException;
import com.acmebank.accountmanager.exception.NegativeAccountBalanceException;
import com.acmebank.accountmanager.model.entity.Account;
import com.acmebank.accountmanager.repository.AccountRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;


@Service
public class AccountService {
    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Transactional(readOnly = true)
    public double getAccountBalance(Long accountNumber) throws InvalidAccountNumberException {
        Account account = accountRepository.findById(accountNumber).orElseThrow(() -> new InvalidAccountNumberException(accountNumber));
        return account.getBalance();
    }

    @Transactional(rollbackFor = Exception.class)
    public void transfer(Long fromAccountNumber, Long toAccountNumber, double amount) throws InvalidAccountNumberException, NegativeAccountBalanceException {
        Assert.isTrue(amount > 0, "Transfer amount should be greater than 0");
        Account fromAccount = accountRepository.findById(fromAccountNumber).orElseThrow(() -> new InvalidAccountNumberException(fromAccountNumber));
        Account toAccount = accountRepository.findById(toAccountNumber).orElseThrow(() -> new InvalidAccountNumberException(toAccountNumber));
        if (fromAccount.debit(amount)) {
            toAccount.credit(amount);
        } else {
            throw new NegativeAccountBalanceException(fromAccount.getAccountNumber(), amount, fromAccount.getBalance());
        }
    }
}
