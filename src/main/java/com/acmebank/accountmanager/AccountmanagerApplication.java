package com.acmebank.accountmanager;

import com.acmebank.accountmanager.model.entity.Account;
import com.acmebank.accountmanager.repository.AccountRepository;
import com.acmebank.accountmanager.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.Optional;

@SpringBootApplication
public class AccountmanagerApplication {

    @Autowired
    AccountRepository accountRepository;

    public static void main(String[] args) {
        SpringApplication.run(AccountmanagerApplication.class, args);
    }

    @PostConstruct
    void init(){
        // init accounts
        Account a = new Account(12345678L, 1000000, "HKD");
        Account b = new Account(88888888L, 1000000, "HKD");
        Optional<Account> optionalA = accountRepository.findById(a.getAccountNumber());
        if(!optionalA.isPresent()){
            accountRepository.save(a);
        }
        Optional<Account> optionalB = accountRepository.findById(b.getAccountNumber());
        if(!optionalB.isPresent()){
            accountRepository.save(b);
        }

    }
}
