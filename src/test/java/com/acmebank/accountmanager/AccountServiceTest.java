package com.acmebank.accountmanager;

import com.acmebank.accountmanager.exception.InvalidAccountNumberException;
import com.acmebank.accountmanager.exception.NegativeAccountBalanceException;
import com.acmebank.accountmanager.model.entity.Account;
import com.acmebank.accountmanager.repository.AccountRepository;
import com.acmebank.accountmanager.service.AccountService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AccountServiceTest {

    @Test
    public void test_getAccountBalance() throws InvalidAccountNumberException {
        AccountRepository accountRepository = mock(AccountRepository.class);
        when(accountRepository.findById(12345678L)).thenReturn(java.util.Optional.of(new Account(12345678L, 1000000, "HKD")));
        AccountService accountService = new AccountService(accountRepository);
        assertEquals(1000000, accountService.getAccountBalance(12345678L));
    }

    @Test
    public void test_getAccountBalance_invalidAccount() {
        AccountRepository accountRepository = mock(AccountRepository.class);
        when(accountRepository.findById(12345678L)).thenReturn(java.util.Optional.empty());
        AccountService accountService = new AccountService(accountRepository);

       assertThrows(InvalidAccountNumberException.class, () -> {
            accountService.getAccountBalance(12345678L);
        });
    }

    @Test
    public void test_transfer_invalidAccount() {
        AccountRepository accountRepository = mock(AccountRepository.class);
        when(accountRepository.findById(12345678L)).thenReturn(java.util.Optional.empty());
        AccountService accountService = new AccountService(accountRepository);

        assertThrows(InvalidAccountNumberException.class, () -> {
            accountService.getAccountBalance(12345678L);
        });
    }

    @Test
    public void test_transfer() throws InvalidAccountNumberException, NegativeAccountBalanceException {
        AccountRepository accountRepository = mock(AccountRepository.class);
        when(accountRepository.findById(12345678L)).thenReturn(java.util.Optional.of(new Account(12345678L, 1000000, "HKD")));
        when(accountRepository.findById(88888888L)).thenReturn(java.util.Optional.of(new Account(88888888L, 1000000, "HKD")));
        AccountService accountService = new AccountService(accountRepository);
        accountService.transfer(12345678L, 88888888L, 1000000);
        assertEquals(0, accountService.getAccountBalance(12345678L));
        assertEquals(2000000, accountService.getAccountBalance(88888888L));
    }

    @Test
    public void test_transfer_exceed_balance() {
        AccountRepository accountRepository = mock(AccountRepository.class);
        when(accountRepository.findById(12345678L)).thenReturn(java.util.Optional.of(new Account(12345678L, 1000000, "HKD")));
        when(accountRepository.findById(88888888L)).thenReturn(java.util.Optional.of(new Account(88888888L, 1000000, "HKD")));
        AccountService accountService = new AccountService(accountRepository);

        assertThrows(NegativeAccountBalanceException.class, () -> {
            accountService.transfer(12345678L, 88888888L, 1000001);
        });
    }

    @Test
    public void test_transfer_nonPositiveAmount() {
        AccountRepository accountRepository = mock(AccountRepository.class);
        when(accountRepository.findById(12345678L)).thenReturn(java.util.Optional.of(new Account(12345678L, 1000000, "HKD")));
        when(accountRepository.findById(88888888L)).thenReturn(java.util.Optional.of(new Account(88888888L, 1000000, "HKD")));
        AccountService accountService = new AccountService(accountRepository);

        Exception exception = assertThrows(Exception.class, () -> {
            accountService.transfer(12345678L, 88888888L, -1);
        });

        Exception exception2 = assertThrows(Exception.class, () -> {
            accountService.transfer(12345678L, 88888888L, 0);
        });

        assertEquals("Transfer amount should be greater than 0", exception.getMessage());
        assertEquals("Transfer amount should be greater than 0", exception2.getMessage());
    }
}
